###*
# Modulo AngularJS para manejo de direcciones de Mexico
# 
# - injection: inyecta 'ngCodigoPostal' en el archivo pricipal de tu aplicacion
#     ejemplo : angular.module 'app', [ ..., 'ngCodigoPostal' ]
#
# - uso de la directiva
#     code :
#       <ng-codigo-postal
#         ng-model="<DOMICILIO_NG_MODEL>">
#       </ng-codigo-postal>
#
#     donde:
#       - DOMICILIO_NG_MODEL es el nombre de la variable a que contiene el model domicilio
###
angular
  .module 'ngCodigoPostal', []
  # provider
  .provider '$codigoPostal', [ ->
    {
      uri: undefined
      init: ( uri ) ->
        @uri = uri.replace /(\/)*$/i, ''
      $get : -> uri : @uri
    }
  ]
  # base uri
  .factory 'cpUri', [
    '$codigoPostal'
    ( $cp ) -> $cp.uri
  ]
  # service
  .service '$cp', [
    '$http'
    'cpUri'
    ( $http, uri ) ->
      {
        codigo : ( codigo ) -> $http.get uri + '/codigo/' + codigo
        entidades : -> $http.get uri + '/entidades'
        municipios : ( entidad ) -> $http.get uri + '/entidad/' + entidad + '/municipios'
        localidades : ( entidad, municipio ) -> $http.get uri + '/entidad/' + entidad + '/municipio/' + municipio + '/localidades'
        paises : -> $http.get uri + '/paises'
      }
  ]
  # directive
  .directive 'ngCodigoPostal', [
    '$cp'
    ( $cp ) ->
      {
        restrict : 'E'
        replace : true
        template : '<TEMPLATE>'
        require : [
          'ngModel'
        ]
        scope :
          ngModel : '='
        controller : [
          '$scope'
          '$cp'
          ( $scope, $cp ) ->

            # initialize entidades
            $cp
              .entidades()
              .then ( response ) ->
                if response.status is 200
                  $scope.entidades = response.data


            getEntidad = ->
              entidad = $scope.ngModel.entidad
              ( $scope.entidades.filter ( el ) -> el.nombre is entidad ).shift()

            getMunicipio = ->
              municipio = $scope.ngModel.municipio
              ( $scope.municipios.filter ( el ) -> el.nombre is municipio ).shift()

            getLocalidad = ->
              localidad = $scope.ngModel.localidad
              ( $scope.localidades.filter ( el ) -> el.nombre is localidad ).shift()

            $scope.loadMunicipios = ->
              # init required params
              entidad = getEntidad()
              return if not entidad
              # reset municipio and localidad
              $scope.ngModel.municipio = undefined
              $scope.ngModel.localidad = undefined
              $scope.ngModel.codigoPostal = undefined
              # get municipios
              $cp
                .municipios entidad.clave
                .then ( response ) ->
                  if response.status is 200
                    $scope.municipios = response.data

            $scope.loadLocalidades = ->
              # init required params
              entidad = getEntidad()
              municipio = getMunicipio()
              return if not entidad or not municipio
              # reset localidades
              $scope.ngModel.localidad = undefined
              # get localidades
              $cp
                .localidades entidad.clave, municipio.clave
                .then ( response ) ->
                  if response.status is 200
                    $scope.localidades = response.data

            $scope.setCodigo = ->
              # init required params
              localidad = getLocalidad()
              return if not localidad
              $scope.ngModel.codigoPostal = localidad.codigo

            $scope.$watch 'ngModel.codigoPostal', ->
              codigo = $scope.ngModel?.codigoPostal
              return if not codigo

              if codigo.length is 5
                $cp
                  .codigo codigo
                  .then ( response ) ->
                    if response.status is 200
                      console.log codigo
                      postal = response.data

                      # set entidad, municipio and colony
                      $scope.ngModel.entidad = postal.estado.nombre
                      $scope.ngModel.municipio = postal.municipio.nombre
                      $scope.ngModel.localidad = postal.localidades[0].nombre if postal.localidades.length is 1

                      # set localidades
                      $scope.localidades = postal.localidades

                      # load municipios
                      $cp
                        .municipios postal.estado.clave
                        .then ( response ) ->
                          if response.status is 200
                            $scope.municipios = response.data

            , true

        ]
        link : ( $scope, $elem, attrs, ctrls ) ->
      }
  ]

