// Generated by CoffeeScript 2.3.2
  /**
   * Modulo AngularJS para manejo de direcciones de Mexico
   * 
   * - injection: inyecta 'ngCodigoPostal' en el archivo pricipal de tu aplicacion
   *     ejemplo : angular.module 'app', [ ..., 'ngCodigoPostal' ]
   *
   * - uso de la directiva
   *     code :
   *       <ng-codigo-postal
   *         ng-model="<DOMICILIO_NG_MODEL>">
   *       </ng-codigo-postal>
   *
   *     donde:
   *       - DOMICILIO_NG_MODEL es el nombre de la variable a que contiene el model domicilio
   */
  // provider
angular.module('ngCodigoPostal', []).provider('$codigoPostal', [
  function() {
    return {
      uri: void 0,
      init: function(uri) {
        return this.uri = uri.replace(/(\/)*$/i,
  '');
      },
      $get: function() {
        return {
          uri: this.uri
        };
      }
    };
  }
// base uri
]).factory('cpUri', [
  '$codigoPostal',
  function($cp) {
    return $cp.uri;
  }
// service
]).service('$cp', [
  '$http',
  'cpUri',
  function($http,
  uri) {
    return {
      codigo: function(codigo) {
        return $http.get(uri + '/codigo/' + codigo);
      },
      entidades: function() {
        return $http.get(uri + '/entidades');
      },
      municipios: function(entidad) {
        return $http.get(uri + '/entidad/' + entidad + '/municipios');
      },
      localidades: function(entidad,
  municipio) {
        return $http.get(uri + '/entidad/' + entidad + '/municipio/' + municipio + '/localidades');
      },
      paises: function() {
        return $http.get(uri + '/paises');
      }
    };
  }
// directive
]).directive('ngCodigoPostal', [
  '$cp',
  function($cp) {
    return {
      restrict: 'E',
      replace: true,
      template: '<div class="is-row"><div class="is-one"><div class="is-row"><div class="is-one"><div class="is-field"><label>Entidad federativa</label><div class="is-select"><select ng-model="ngModel.entidad" ng-change="loadMunicipios()" required="required"><option selected="selected" disabled="disabled" value="">Selecciona entidad</option><option ng-repeat="entidad in entidades">{{ entidad.nombre }}</option></select></div></div></div><div class="is-one"><div class="is-field"><label>Municipio</label><div class="is-select"><select ng-model="ngModel.municipio" ng-change="loadLocalidades()" required="required"><option selected="selected" disabled="disabled" value="">Selecciona municipio</option><option ng-repeat="municipio in municipios">{{ municipio.nombre }}</option></select></div></div></div></div><div class="is-row"><div class="is-one"><div class="is-field"><label>Localidad / Colonia</label><div class="is-select"><select ng-model="ngModel.localidad" ng-change="setCodigo()" required="required"><option selected="selected" disabled="disabled" value="">Selecciona colonia</option><option ng-repeat="localidad in localidades">{{ localidad.nombre }}</option></select></div></div></div><div class="is-one"><div class="is-field"><label>C.P.</label><div class="is-control"><input type="text" ng-model="ngModel.codigoPostal" required="required"/></div></div></div></div></div></div>',
      require: ['ngModel'],
      scope: {
        ngModel: '='
      },
      controller: [
        '$scope',
        '$cp',
        function($scope,
        $cp) {
          var getEntidad,
        getLocalidad,
        getMunicipio;
          // initialize entidades
          $cp.entidades().then(function(response) {
            if (response.status === 200) {
              return $scope.entidades = response.data;
            }
          });
          getEntidad = function() {
            var entidad;
            entidad = $scope.ngModel.entidad;
            return ($scope.entidades.filter(function(el) {
              return el.nombre === entidad;
            })).shift();
          };
          getMunicipio = function() {
            var municipio;
            municipio = $scope.ngModel.municipio;
            return ($scope.municipios.filter(function(el) {
              return el.nombre === municipio;
            })).shift();
          };
          getLocalidad = function() {
            var localidad;
            localidad = $scope.ngModel.localidad;
            return ($scope.localidades.filter(function(el) {
              return el.nombre === localidad;
            })).shift();
          };
          $scope.loadMunicipios = function() {
            var entidad;
            // init required params
            entidad = getEntidad();
            if (!entidad) {
              return;
            }
            // reset municipio and localidad
            $scope.ngModel.municipio = void 0;
            $scope.ngModel.localidad = void 0;
            $scope.ngModel.codigoPostal = void 0;
            // get municipios
            return $cp.municipios(entidad.clave).then(function(response) {
              if (response.status === 200) {
                return $scope.municipios = response.data;
              }
            });
          };
          $scope.loadLocalidades = function() {
            var entidad,
        municipio;
            // init required params
            entidad = getEntidad();
            municipio = getMunicipio();
            if (!entidad || !municipio) {
              return;
            }
            // reset localidades
            $scope.ngModel.localidad = void 0;
            // get localidades
            return $cp.localidades(entidad.clave,
        municipio.clave).then(function(response) {
              if (response.status === 200) {
                return $scope.localidades = response.data;
              }
            });
          };
          $scope.setCodigo = function() {
            var localidad;
            // init required params
            localidad = getLocalidad();
            if (!localidad) {
              return;
            }
            return $scope.ngModel.codigoPostal = localidad.codigo;
          };
          return $scope.$watch('ngModel.codigoPostal',
        function() {
            var codigo,
        ref;
            codigo = (ref = $scope.ngModel) != null ? ref.codigoPostal : void 0;
            if (!codigo) {
              return;
            }
            if (codigo.length === 5) {
              return $cp.codigo(codigo).then(function(response) {
                var postal;
                if (response.status === 200) {
                  console.log(codigo);
                  postal = response.data;
                  // set entidad, municipio and colony
                  $scope.ngModel.entidad = postal.estado.nombre;
                  $scope.ngModel.municipio = postal.municipio.nombre;
                  if (postal.localidades.length === 1) {
                    $scope.ngModel.localidad = postal.localidades[0].nombre;
                  }
                  // set localidades
                  $scope.localidades = postal.localidades;
                  // load municipios
                  return $cp.municipios(postal.estado.clave).then(function(response) {
                    if (response.status === 200) {
                      return $scope.municipios = response.data;
                    }
                  });
                }
              });
            }
          },
        true);
        }
      ],
      link: function($scope,
  $elem,
  attrs,
  ctrls) {}
    };
  }
]);
